import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Products } from "../entities/Products";

export const getProducts = async (req: Request,res: Response): Promise<Response> => {
    let page = parseInt(req.params.page)
    if(req.params.id.toString() === "none"){
        if(page === 1) {
            const [results, count] = await getRepository(Products).findAndCount({ order:{ name: "ASC"}, take: 10});
            return res.json({result: results, count: count});
        }else{
            page = 10*(page-1)
            const [results, count] = await getRepository(Products).findAndCount({ order:{ name: "ASC"}, skip: page, take: 10} );
            return res.json({result: results, count: count});
        }
    }
    if(req.params.id.toString() === "lowestPrice"){
        if(page === 1) {
            const [results, count] = await getRepository(Products).findAndCount({ order:{ price: "ASC"}, take: 10});
            return res.json({result: results, count: count});
        }else{
            page = 10*(page-1)
            const [results, count] = await getRepository(Products).findAndCount({ order:{ price: "ASC"}, skip: page, take: 10} );
            return res.json({result: results, count: count});
        }
    }
    if(req.params.id.toString() === "mostRecent"){
        if(page === 1) {
            const [results, count] = await getRepository(Products).findAndCount({ order:{ date: "ASC"}, take: 10});
            return res.json({result: results, count: count});
        }else{
            page = 10*(page-1)
            const [results, count] = await getRepository(Products).findAndCount({ order:{ date: "ASC"}, skip: page, take: 10} );
            return res.json({result: results, count: count});
        }
    }
    return res.json(null)
};