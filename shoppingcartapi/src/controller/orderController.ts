import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Order } from "../entities/Order";

export const getOrder = async (req: Request,res: Response): Promise<Response> => {
    const results = await getRepository(Order).findOne(req.params.id);
    return res.json(results);
};

export const createOrder = async (req: Request,res: Response): Promise<Response> => {
    const newOrder = await getRepository(Order).create(req.body);
    const results = await getRepository(Order).save(newOrder);
    return res.json(results);
};