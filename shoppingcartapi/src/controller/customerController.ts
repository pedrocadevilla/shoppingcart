import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Customer } from "../entities/Customer";

export const getCustomer = async (req: Request,res: Response): Promise<Response> => {
    const results = await getRepository(Customer).findOne({ where: { email: req.params.email.toString( ) } });
    return res.json(results);
};

export const createCustomer = async (req: Request,res: Response): Promise<Response> => {
    const newCustomer = await getRepository(Customer).create(req.body);
    const results = await getRepository(Customer).save(newCustomer);
    return res.json(results);
};