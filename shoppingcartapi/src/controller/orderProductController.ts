import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { OrderProducts } from "../entities/OrderProducts";

export const createOrderProduct = async (req: Request,res: Response): Promise<Response> => {
    const newOrder = await getRepository(OrderProducts).create(req.body);
    const results = await getRepository(OrderProducts).save(newOrder);
    return res.json(results);
};