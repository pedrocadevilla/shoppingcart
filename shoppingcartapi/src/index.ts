import express from 'express';
import morgan from 'morgan';
import cors from 'cors'
import { createConnection } from 'typeorm'

import userRoutes from './routes/customerRoute'
import orderRoutes from './routes/orderRoute'
import orderProductRoutes from './routes/orderProductRoute'
import productRoutes from './routes/productRoute'

const app = express();
createConnection();

app.use(cors());
app.use(express.json());
app.use(morgan('dev'));

app.use(userRoutes, orderRoutes, productRoutes, orderProductRoutes);

app.listen(3001);
console.log('Server on port', 3001);