import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Products } from "./Products";
import { Order } from "./Order";

@Index("ProductoFK_idx", ["productId"], {})
@Index("OrderFK_idx", ["orderId"], {})
@Entity("order_products", { schema: "shoppingcart" })
export class OrderProducts {
  @PrimaryGeneratedColumn({ type: "bigint", name: "Id" })
  id: string;

  @Column("bigint", { name: "OrderId", nullable: true })
  orderId: string | null;

  @Column("bigint", { name: "ProductId", nullable: true })
  productId: string | null;

  @Column("bigint", { name: "Quantity", nullable: true })
  quantity: string | null;

  @ManyToOne(() => Products, (products) => products.orderProducts, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "ProductId", referencedColumnName: "id" }])
  product: Products;

  @ManyToOne(() => Order, (order) => order.orderProducts, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "OrderId", referencedColumnName: "id" }])
  order: Order;
}
