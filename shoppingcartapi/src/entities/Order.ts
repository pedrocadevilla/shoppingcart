import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Customer } from "./Customer";
import { OrderProducts } from "./OrderProducts";

@Index("CustomerFK_idx", ["customerId"], {})
@Entity("order", { schema: "shoppingcart" })
export class Order {
  @PrimaryGeneratedColumn({ type: "bigint", name: "Id" })
  id: string;

  @Column("bigint", { name: "CustomerId", nullable: true })
  customerId: string | null;

  @ManyToOne(() => Customer, (customer) => customer.orders, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "CustomerId", referencedColumnName: "id" }])
  customer: Customer;

  @OneToMany(() => OrderProducts, (orderProducts) => orderProducts.order)
  orderProducts: OrderProducts[];
}
