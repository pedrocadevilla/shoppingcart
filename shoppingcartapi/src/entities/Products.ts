import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { OrderProducts } from "./OrderProducts";

@Entity("products", { schema: "shoppingcart" })
export class Products {
  @PrimaryGeneratedColumn({ type: "bigint", name: "Id" })
  id: string;

  @Column("text", { name: "Image" })
  image: string;

  @Column("varchar", { name: "Name", length: 45 })
  name: string;

  @Column("varchar", { name: "Price", length: 45 })
  price: string;

  @Column("varchar", { name: "Category", length: 45 })
  category: string;

  @Column("varchar", { name: "Description", length: 150 })
  description: string;

  @Column("bigint", { name: "Stock" })
  stock: string;

  @Column("date", { name: "Date" })
  date: string;

  @OneToMany(() => OrderProducts, (orderProducts) => orderProducts.product)
  orderProducts: OrderProducts[];
}
