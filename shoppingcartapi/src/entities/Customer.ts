import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Order } from "./Order";

@Entity("customer", { schema: "shoppingcart" })
export class Customer {
  @PrimaryGeneratedColumn({ type: "bigint", name: "Id" })
  id: string;

  @Column("varchar", { name: "Name", length: 45 })
  name: string;

  @Column("varchar", { name: "Email", length: 45 })
  email: string;

  @Column("bigint", { name: "Identification" })
  identification: string;

  @Column("varchar", { name: "Address", length: 150 })
  address: string;

  @Column("varchar", { name: "PhoneNumber", length: 45 })
  phoneNumber: string;

  @OneToMany(() => Order, (order) => order.customer)
  orders: Order[];
}
