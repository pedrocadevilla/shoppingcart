import { Router } from "express";
const router = Router();

import {
    getOrder,
    createOrder
} from "../controller/orderController";

router.get("/order/:id", getOrder);
router.post("/order", createOrder);

export default router;