import { Router } from "express";
const router = Router();

import {
    getCustomer,
    createCustomer
} from "../controller/customerController";

router.get("/customer/:email", getCustomer);
router.post("/customer", createCustomer);

export default router;