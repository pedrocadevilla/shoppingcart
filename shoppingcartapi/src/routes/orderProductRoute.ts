import { Router } from "express";
const router = Router();

import {
    createOrderProduct
} from "../controller/orderProductController";

router.post("/orderProduct", createOrderProduct);

export default router;