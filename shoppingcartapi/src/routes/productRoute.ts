import { Router } from "express";
const router = Router();

import {
    getProducts
} from "../controller/productController";

router.get("/getProducts/:id/:page", getProducts);

export default router;