import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import {ThemeProvider} from '@material-ui/core/styles'
import NavBar from './components/navBar'
import { Index } from './pages/index'
import { About } from './pages/about'
import { Contact } from './pages/contact'
import { NotFound } from './pages/notFound'
import { Layout } from './components/layout'
import { CheckOut } from './pages/checkout'
import { PlaceOrder } from './pages/placeOrder'
import { Thanks } from './pages/thanks'
import theme from './theme'

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <NavBar/>
        <React.Fragment>
          <Layout>
              <Switch>
                <Route path="/placeOrder">
                  <PlaceOrder />
                </Route>
                <Route path="/thanks">
                  <Thanks />
                </Route>
                <Route path="/checkout">
                  <CheckOut />
                </Route>
                <Route path="/contact">
                  <Contact/>
                </Route>
                <Route path="/about">
                  <About/>
                </Route>
                <Route exact path="/">
                  <Index/>
                </Route>
                <Route component={NotFound} />
              </Switch>
          </Layout>
        </React.Fragment>
      </Router>
    </ThemeProvider>
  );
}

export default App;
