import React from 'react';
import { Grid } from '@material-ui/core'
import PlaceOrderData from '../components/placeOrderData';

export const PlaceOrder = () => (
    <div>
        <Grid container>
            <PlaceOrderData/>
        </Grid>
    </div>
)