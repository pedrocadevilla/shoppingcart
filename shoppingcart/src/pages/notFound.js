import React from 'react'
import NotFoundText from '../components/notFoundText'

export const NotFound = () => (
	<div>
		<NotFoundText/>
	</div>
)