import React from 'react';
import Sorter from '../components/sorter'
import ProductList from '../components/productList'
import Paginator from '../components/paginator'

export const Index = () => (
    <div>
        <Sorter/>
        <ProductList/>
        <Paginator/>
    </div>
)