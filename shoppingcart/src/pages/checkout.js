import React from 'react';
import { Grid } from '@material-ui/core'
import Summary from '../components/summary';
import CustomerInformation from '../components/customerInformation'

export const CheckOut = (props) => (
    <div>
        <Grid container>
            <Grid item xs={12} sm={12} md={6} lg={6} xl={4}>
                <CustomerInformation/>
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={6} xl={4}>
                <Summary/>
            </Grid>
        </Grid>
    </div>
)

export default CheckOut;