import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import { compose, combineReducers } from 'redux';
import product from '../slice/index'
import customer from '../slice/customerData'
import customerRegistry from '../slice/customerRegistry'
import storage from 'redux-persist/lib/storage'
import {persistReducer} from 'redux-persist'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'

function loggerMiddleware(store){
    return function(next){
        return function(action){
            return next(action);
        }
    }
}

const persistConfig = {
    key: 'root',
    storage,
    stateReconciler: autoMergeLevel2
};

const reducer = combineReducers({
    product,
    customer,
    customerRegistry
})

const persistedReducer = persistReducer(persistConfig, reducer)

const store = configureStore({
    reducer: {
        persistedReducer
    },
    middleware: [...getDefaultMiddleware(), loggerMiddleware],
    enhancers: [compose],
})

export default store