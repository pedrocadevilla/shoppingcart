import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App.jsx';
import { Provider } from 'react-redux';
import store from "./store/store";
import { getProducts } from './actions/index'
import { PersistGate } from 'redux-persist/integration/react'
import { persistStore } from 'redux-persist'

const uri = "http://localhost:3001/getProducts/none/1"
store.dispatch(getProducts(uri))
let persistor = persistStore(store)

ReactDOM.render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>,
  document.getElementById('root')
);