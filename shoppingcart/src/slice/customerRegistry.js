import { createSlice } from '@reduxjs/toolkit'
import { addCustomerData } from '../actions/customerRegistry'

const customerRegistrySlice = createSlice({
    name: "customerRegistry",
    initialState: {
        name: "",
        identification: 0,
        address: "",
        phoneNumber: "",
        email: "",
        id: 0,
        errorRegistry: false
    },
    reducers: {
        putError: (state) => {
            state.errorRegistry = true;
        },
        removeError: (state) => {
            state.errorRegistry = false;
        },
        nameChange: (state, action) => {
            state.name = action.payload;
        },
        idChange: (state, action) => {
            state.identification = action.payload;
        },
        addressChange: (state, action) => {
            state.address = action.payload;
        },
        phoneNumberChange: (state, action) => {
            state.phoneNumber = action.payload;
        },
        emailChange: (state, action) => {
            state.email = action.payload;
        }
    },
    extraReducers: {
        [addCustomerData.rejected]: (state) => {
            state.loading = false;
            state.errorRegistry = true;
        },
        [addCustomerData.fulfilled]: (state, action) => {
            state.errorRegistry = false;
        }
    }
})

export const { nameChange, lastNameChange, idChange, addressChange, phoneNumberChange, emailChange, putError, removeError } = customerRegistrySlice.actions
export default customerRegistrySlice.reducer