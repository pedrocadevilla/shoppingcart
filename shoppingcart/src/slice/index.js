import { createSlice } from '@reduxjs/toolkit'
import { getProducts } from '../actions/index'
import { addOrderProductData } from '../actions/orderRegistry'

const productSlice = createSlice({
    name: "product",
    initialState: {
        products: [],
        addedProducts: [],
        page: 1,
        sorter: "none",
        loading: true,
        error: "",
        countProducts: 0,
        totalPrice: 0.0,
        stockOut: false,
        orderId: 0,
        showRegistry: false
    },
    reducers: {
        addProduct: (state, action) => {
            let json = JSON.parse(JSON.stringify(action.payload));
            json.added = 1;
            state.totalPrice = state.totalPrice + parseFloat(json.price);
            state.addedProducts.push(json);
        },
        removeProduct: (state, action) => {
            let json = JSON.parse(JSON.stringify(action.payload));
            let index = state.addedProducts.findIndex(function(x){
                return x.id === json.id;
            })
            if (index > -1) {
                state.addedProducts.splice(index, 1);
                state.totalPrice = state.totalPrice - parseFloat(json.price);
            }
        },
        quantityAdd: (state, action) => {
            let index = state.addedProducts.findIndex(function(x){
                return x.id === action.payload;
            })
            if(state.addedProducts[index].added+1 < state.addedProducts[index].stock){
                let total = state.totalPrice + parseFloat(state.addedProducts[index].price)
                state.addedProducts[index].added++;
                state.totalPrice = total;
            }else{
                state.stockOut = true;
            }
        },
        quantityRemove: (state, action) => {
            let index = state.addedProducts.findIndex(function(x){
                return x.id === action.payload;
            })
            if(state.addedProducts[index].added-1 >= 1){
                let total = state.totalPrice - parseFloat(state.addedProducts[index].price)
                state.addedProducts[index].added--;
                state.totalPrice = total;
            }
        },
        paginatorChange: (state, action) => {
            state.page = action.payload;
        },
        closeAlert: (state) => {
            state.stockOut = false;
        },
        orderIdChange: (state, action) => {
            state.orderId = action.payload;
        }
    },
    extraReducers: {
        [getProducts.pending]: state => {
            state.loading = true
        },
        [getProducts.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.payload;
        },
        [getProducts.fulfilled]: (state, action) => {
            state.loading = false;
            state.products = action.payload.result;
            state.countProducts = action.payload.count;
        },
        [addOrderProductData.rejected]: (state, action) => {
        },
        [addOrderProductData.fulfilled]: (state, action) => {
            state.showRegistry = true;
        }
    }
})

export const { addProduct, removeProduct, quantityRemove, quantityAdd, paginatorChange, closeAlert, orderIdChange } = productSlice.actions
export default productSlice.reducer