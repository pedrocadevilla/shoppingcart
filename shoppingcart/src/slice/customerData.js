import { createSlice } from '@reduxjs/toolkit'
import { getCustomerData } from '../actions/customerData'
import { addOrderData } from '../actions/orderRegistry'

const customerSlice = createSlice({
    name: "customer",
    initialState: {
        name: "",
        lastName: "",
        id: 0,
        identification: "",
        address: "",
        phoneNumber: "",
        email: "",
        errorSearch: false,
        errorRegistry: false,
        dataResultVisible: false,
        dataResultNotFound: false,
        radioValue: "new",
        selectValue: "none",
        registryVisible: true,
        dataSearchVisible: false,
        inputSearch: "",
        orderId: 0
    },
    reducers: {
        putError: (state) => {
            state.errorRegistry = true;
        },
        removeError: (state) => {
            state.errorRegistry = false;
        },
        putNullEmail: (state) => {
            state.errorSearch = true;
            state.dataResultNotFound = false;
        },
        radioChange: (state, action) => {
            state.radioValue = action.payload;
            if(action.payload === 'new'){
                state.registryVisible = true;
                state.dataSearchVisible = false;
                state.dataResultVisible = false;
                state.dataResultNotFound = false;
            }
            else
            {
                state.registryVisible = false;
                state.dataSearchVisible = true;
                state.dataResultVisible = false;
                state.dataResultNotFound = false;
            }
        },
        selectChange: (state, action) => {
            state.selectValue = action.payload;
        },
        inputChange: (state, action) => {
            state.inputSearch = action.payload;
        }
    },
    extraReducers: {
        [getCustomerData.pending]: state => {
            state.loading = true
        },
        [getCustomerData.rejected]: (state, action) => {
            state.loading = false;
            state.errorSearch = false;
            state.dataResultNotFound = true;
        },
        [getCustomerData.fulfilled]: (state, action) => {
            state.loading = false;
            state.errorSearch = false;
            state.dataResultNotFound = false;
            state.name = action.payload.name;
            state.lastName = action.payload.lastName;
            state.id = action.payload.id;
            state.address = action.payload.address;
            state.phoneNumber = action.payload.phoneNumber;
            state.email = action.payload.email;
            state.identification = action.payload.identification;
            state.dataResultVisible = true;
            state.registryVisible = false;
            state.dataSearchVisible = true;
            state.radioValue = "registered";
        },
        [addOrderData.rejected]: (state, action) => {
            state.errorRegistry = true;
        },
        [addOrderData.fulfilled]: (state, action) => {
            state.orderId = action.payload.id;
        },
    }
})

export const { radioChange, selectChange, inputChange, putNullEmail, putError, removeError } = customerSlice.actions
export default customerSlice.reducer