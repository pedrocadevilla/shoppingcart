import {createMuiTheme} from '@material-ui/core/styles'
import red from '@material-ui/core/colors/red';
import deepOrange from '@material-ui/core/colors/deepOrange';

const theme = createMuiTheme({
    palette: {
        primary: red,
        secondary: deepOrange,
    },
    container: {
        maxWidth: "lg",
    }
})

export default theme;