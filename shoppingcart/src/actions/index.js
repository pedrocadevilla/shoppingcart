const { createAsyncThunk } = require("@reduxjs/toolkit");

export const getProducts = createAsyncThunk(
    "product/getProducts",
    (endpoint) => {
        return fetch(endpoint)
        .then(response => {
            if(!response.ok) throw Error(response.statusText);
            return response.json();
        })
        .then(json => json);
    }
)