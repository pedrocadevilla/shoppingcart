const { createAsyncThunk } = require("@reduxjs/toolkit");

export const addOrderData = createAsyncThunk(
    "order/addOrderData",
    (body) => {
        let obj = {"customerId": body};
        let bodyFetch = JSON.stringify(obj);
        return fetch("http://localhost:3001/order", {method:'POST', body: bodyFetch, headers: {"Content-Type": "application/json"}})
        .then(response => {
            if(!response.ok) throw Error(response.statusText);
            return response.json();
        })
        .then(json => json);
    }
)

export const addOrderProductData = createAsyncThunk(
    "order/addOrderProductData",
    (body) => {
        let orderId = body.orderId;
        let objFinal = [];
        body.addedProducts.forEach(x => {
            let obj = {};
            obj.productId = x.id;
            obj.orderId = orderId;
            obj.quantity = x.added;
            objFinal.push(obj);
        });
        let bodyFetch = JSON.stringify(objFinal);
        return fetch("http://localhost:3001/orderProduct", {method:'POST', body: bodyFetch, headers: {"Content-Type": "application/json"}})
        .then(response => {
            if(!response.ok) throw Error(response.statusText);
            return response.json();
        })
        .then(json => json);
    }
)