const { createAsyncThunk } = require("@reduxjs/toolkit");

export const addCustomerData = createAsyncThunk(
    "customer/addCustomerData",
    (body) => {
        let a = JSON.stringify(body);
        return fetch("http://localhost:3001/customer", {method:'POST', body: a, headers: {"Content-Type": "application/json"}})
        .then(response => {
            if(!response.ok) throw Error(response.statusText);
            return response.json();
        })
        .then(json => json);
    }
)