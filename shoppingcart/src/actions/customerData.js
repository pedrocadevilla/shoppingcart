const { createAsyncThunk } = require("@reduxjs/toolkit");
const uri = "http://localhost:3001/customer/"

export const getCustomerData = createAsyncThunk(
    "customer/getCustomerData",
    (email) => {
        return fetch(uri+email)
        .then(response => {
            if(!response.ok) throw Error(response.statusText);
            return response.json();
        })
        .then(json => json);
    }
)