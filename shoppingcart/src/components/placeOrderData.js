import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Typography, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Snackbar } from '@material-ui/core';
import { quantityAdd, quantityRemove, closeAlert } from "../slice/index"
import { connect } from "react-redux"
import { Link } from "react-router-dom";
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import { Alert } from '@material-ui/lab';

const properStyles = makeStyles((theme) => ({
    button: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    centered: {
        display: 'flex',
        width: '100%',
        justifyContent:'center'
    },
    textTotal:{
        padding: theme.spacing(2)
    },
    remove: {
        marginRight: theme.spacing(1),
        cursor: 'pointer'
    },
    add: {
        marginLeft: theme.spacing(1),
        cursor: 'pointer'
    }
}))

export const PlaceOrderData = (props) => {
    const classes = properStyles()
    return (
        <div className={classes.centered}>
            <Grid container >
                <Snackbar open={props.state.persistedReducer.product.stockOut} autoHideDuration={5000} onClose={() => props.closeAlert()}>
                    <Alert onClose={() => props.closeAlert()} severity="error">
                        No more products in stock
                    </Alert>
                </Snackbar>
                <Button variant="text" color="default">
                </Button>
                <Grid container justify="flex-end" className={classes.button}>
                    <Link className="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary" to ='/checkout' >
                        CheckOut
                    </Link>
                </Grid>
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                        <TableRow>
                            <TableCell>Product</TableCell>
                            <TableCell align="right">Unit Price</TableCell>
                            <TableCell align="right">Quantity</TableCell>
                            <TableCell align="right">Total Price</TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {props.state.persistedReducer.product.addedProducts.map((product) => (
                            <TableRow key={product.id}>
                                <TableCell component="th" scope="row">{product.name}</TableCell>
                                <TableCell align="right">{parseFloat(product.price)} $</TableCell>
                                <TableCell align="right">
                                    <Grid container justify="flex-end">
                                        <RemoveIcon className={classes.remove} onClick={(event) => props.quantityRemove(product.id)}/>
                                        <Typography variant="subtitle1" gutterBottom >
                                            {product.added}
                                        </Typography>
                                        <AddIcon className={classes.add} onClick={(event) => props.quantityAdd(product.id)}/>
                                    </Grid>
                                </TableCell>
                                <TableCell align="right">{parseFloat(product.price).toFixed(2) * product.added} $</TableCell>
                            </TableRow>
                        ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Grid container justify="flex-end" className={classes.textTotal}>
                    <Typography variant="subtitle1" gutterBottom>
                        Total: {props.state.persistedReducer.product.totalPrice} $
                    </Typography>
                </Grid>
                <Grid container justify="flex-end" className={classes.button}>
                    <Link className="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary" to ='/checkout' >
                        CheckOut
                    </Link>
                </Grid>
            </Grid>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        quantityAdd: (val) => {
            dispatch(quantityAdd(val))
        },
        quantityRemove: (val) => {
            dispatch(quantityRemove(val))
        },
        closeAlert: () => {
            dispatch(closeAlert())
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlaceOrderData);