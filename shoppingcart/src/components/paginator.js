import React from 'react'
import { Grid, makeStyles } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import { connect } from "react-redux";
import { paginatorChange } from "../slice/index"
import { getProducts } from '../actions/index'

const properStyles = makeStyles(theme => ({
    pagination: {
        margin: theme.spacing(2),
    }
}))

export const Paginator = (props) => {
    const classes = properStyles()
    return (
        <div>
            <Grid container justify="center" className={classes.pagination}>
                <Pagination count={Math.floor(props.state.persistedReducer.product.countProducts/10)} variant="outlined" shape="rounded" color="primary" onChange={(event, val) => props.paginatorChange(val, props)}/>
            </Grid>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        paginatorChange: (val, props) => {
            dispatch(paginatorChange(val))
            dispatch(getProducts("http://localhost:3001/getProducts/"+props.state.persistedReducer.customer.selectValue+"/"+val))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Paginator);
