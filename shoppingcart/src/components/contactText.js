import React from 'react'
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography'

const properStyles = makeStyles(theme => ({
    centerText: {
        textAlign: 'center'
    }
}))

export const ContactText = () => {
    const classes = properStyles()
    return (
        <div className={classes.centerText}>
            <Typography variant="h3" paragraph>
                CONTACT
            </Typography>
            <Typography variant="subtitle1" gutterBottom>
                Developed by Pedro Luis Cadevilla Chacon
            </Typography>
            <Typography variant="subtitle1" gutterBottom>
                Email: pedrique27@gmail.com
            </Typography>
            <Typography variant="subtitle1" gutterBottom>
                Phone-Number: +584265753457
            </Typography>
            <Typography variant="subtitle1" gutterBottom>
                Country: Venezuela
            </Typography>
        </div>
    )
}

export default ContactText;