import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Typography} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { connect } from "react-redux"
import { addOrderData } from '../actions/orderRegistry'
import { putError } from "../slice/customerData"
import { orderIdChange } from "../slice/index"
import { addOrderProductData } from '../actions/orderRegistry'
import { Link } from "react-router-dom"

const properStyles = makeStyles((theme) => ({
    table: {
        maxWidth: '90%',
        marginLeft: '10%'
    },
    buttonMargin: {
        marginTop: theme.spacing(2)
    },
    textTotal:{
        padding: theme.spacing(2)
    }
}))

export const Summary = (props) => {
    const classes = properStyles()
    return (
        <div>
            <TableContainer component={Paper} className={classes.table}>
            { props.state.persistedReducer.customer.errorRegistry && <Alert severity="error">You have to find or create a customer</Alert>}
                <Table aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        <TableCell>Product</TableCell>
                        <TableCell align="right">Unit Price</TableCell>
                        <TableCell align="right">Units</TableCell>
                        <TableCell align="right">Total Price</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {props.state.persistedReducer.product.addedProducts.map((product) => (
                        <TableRow key={product.id}>
                        <TableCell component="th" scope="row">
                            {product.name}
                        </TableCell>
                        <TableCell align="right">{product.price}</TableCell>
                        <TableCell align="right">{product.added}</TableCell>
                        <TableCell align="right">{parseFloat(product.price).toFixed(2) * product.added} $</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                <Grid container justify="flex-end" className={classes.textTotal}>
                    <Typography variant="subtitle1" gutterBottom>
                        Total: {props.state.persistedReducer.product.totalPrice} $
                    </Typography>
                </Grid>
            </TableContainer>
            <Grid container justify="flex-end" className={classes.buttonMargin}>
                <Button>
                </Button>
                <Link className="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary" onClick={(event) => props.addOrderData(props.state.persistedReducer.customer, props.state.persistedReducer.product)} to ='/thanks' >
                    PLace Order
                </Link>
            </Grid>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        addOrderData: (obj, obj2) => {
            if(obj.id !== 0){
                dispatch(addOrderData(obj.id))
                dispatch(orderIdChange(obj.orderId))
                dispatch(addOrderProductData(obj2))
            }else{
                dispatch(putError())
            }
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Summary);