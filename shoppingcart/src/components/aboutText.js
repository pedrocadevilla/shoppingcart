import React from 'react'
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography'

const properStyles = makeStyles(theme => ({
    centerText: {
        textAlign: 'center'
    }
}))

export const AboutText = () => {
    const classes = properStyles()
    return (
        <div className={classes.centerText}>
            <Typography variant="h3" paragraph>
                ABOUT
            </Typography>
            <Typography variant="subtitle1" gutterBottom>
                The current app was developed using Node - Express for the server side
            </Typography>
            <Typography variant="subtitle1" gutterBottom>
                For the client side I used React, Redux, Persist, ReduxToolkit and MaterialUI
            </Typography>
        </div>
    )
}

export default AboutText;