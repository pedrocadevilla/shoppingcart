import React from 'react'
import { makeStyles, Grid, Button } from '@material-ui/core';
import Typography from '@material-ui/core/Typography'
import { connect } from "react-redux";
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import { Link } from "react-router-dom";

const properStyles = makeStyles(theme => ({
    centerText: {
        textAlign: 'center'
    },
    iconBig: {
        fontSize: '100px'
    }
}))

export const ThanksText = (props) => {
    const classes = properStyles()
    return (
        <div className={classes.centerText}>
            <SentimentVerySatisfiedIcon className={classes.iconBig}/>
            <Typography variant="h3" paragraph>
                Thank for your purchase
            </Typography>
            <Typography variant="subtitle1" gutterBottom>
                { props.state.persistedReducer.customer.name }, we haver created your order {props.state.persistedReducer.customer.orderId}
            </Typography>
            <Typography variant="subtitle1" paragraph>
                Your items will be soon at your door stay safe
            </Typography>
            <Grid container justify="center">
                <Link className="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary" to ='/' >
                    Start Again
                </Link>
            </Grid>
            <Button>
            </Button>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

export default connect(mapStateToProps)(ThanksText);