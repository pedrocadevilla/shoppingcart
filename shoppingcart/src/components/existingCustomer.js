import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { FormHelperText, Grid, CircularProgress } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import FormControl from '@material-ui/core/FormControl'
import { getCustomerData } from '../actions/customerData'
import { putNullEmail } from '../slice/customerData'
import { inputChange } from "../slice/customerData"
import { connect } from "react-redux"
import { Alert } from '@material-ui/lab';

const properStyles = makeStyles((theme) => ({
    inputEmail: {
        minWidth: '100%',
        display: 'flex'
    }
}))

export const ExistingCustomer = (props) => {
    const classes = properStyles()

    return (
        <div>
            { props.state.persistedReducer.customer.loading && <CircularProgress color="secondary" />}
            { props.state.persistedReducer.customer.errorSearch && <Alert severity="error">You have to enter an email</Alert>}
            { props.state.persistedReducer.customer.dataResultNotFound && <Alert severity="error">USER NOT FOUND IN THE SYSTEM</Alert> }
            <FormControl className={classes.inputEmail}>
                <TextField id="emailSearch" label="Email" variant="outlined"  onChange={(event) => props.inputChange(event.target.value)}/>
                <FormHelperText>Enter the email of the customer</FormHelperText>
            </FormControl>
            <Grid container justify="flex-end">
                <Button variant="contained" color="secondary" onClick={(event) => props.getCustomerData(props.state.persistedReducer.customer.inputSearch)}>
                    Search
                </Button>
            </Grid>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        getCustomerData: (val) => {
            if(val !== ""){
                dispatch(getCustomerData(val))
            }else{
                dispatch(putNullEmail(val))
            }
        },
        inputChange: (val) => {
            dispatch(inputChange(val))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExistingCustomer);