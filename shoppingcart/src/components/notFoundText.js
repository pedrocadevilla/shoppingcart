import React from 'react'
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography'

const properStyles = makeStyles(theme => ({
    centerText: {
        textAlign: 'center'
    }
}))

export const NotFoundText = () => {
    const classes = properStyles()
    return (
        <div className={classes.centerText}>
            <Typography variant="h2">
                404 NOT FOUND
            </Typography>
        </div>
    )
}

export default NotFoundText;