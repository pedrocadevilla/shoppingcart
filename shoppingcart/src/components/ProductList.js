import React from 'react'
import { Grid, makeStyles, IconButton, CircularProgress, Card, CardContent } from '@material-ui/core'
import { Typography, Box, CardMedia } from '@material-ui/core/'
import AddCircleOutlinedIcon from '@material-ui/icons/AddCircleOutlined'
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle'
import { connect } from "react-redux";
import { addProduct, removeProduct } from "../slice/index"

const properStyles = makeStyles(theme => ({
    cover: {
        maxWidth: 160,
        minHeight: 160,
        maxHeight: 160,
        minWidth: 160,
        display: 'flex',
        flexDirection: 'column',
    },
    addB: {
        color: '#00A152',
    },
    root: {
        display: 'flex',
        spacing: 2,
    },
    detail: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        maxHeight: 160,
        position: 'relative'
    },
    containerProduct:{
        padding: theme.spacing(2)
    },
    contentCard: {
        padding: 5,
    },
    priceButton: {
        position: 'absolute',
        bottom: 0
    }
}))

export const ProductList = (props) => {
    const classes = properStyles()
    return (
        <div>
            <Grid container>
                { props.state.persistedReducer.product.loading && <CircularProgress color="secondary" />}
                { props.state.persistedReducer.product.products.length > 0 &&
                    props.state.persistedReducer.product.products.map((product) => (
                        <Grid key={product.id} item xs={12} sm={12} md={6} lg={6} xl={4} className={classes.containerProduct}>
                        <Card className={classes.root}>
                            <CardMedia
                                component="img"
                                className={classes.cover}
                                image={product.image}
                            />
                            <div className={classes.detail}>
                                <CardContent className={classes.contentCard}>
                                    <Typography component="h5" variant="h5">
                                        {product.name}
                                    </Typography>
                                    <Typography variant="subtitle1" color="textSecondary">
                                        {product.category}
                                    </Typography>
                                    <Typography variant="subtitle2" color="textSecondary">
                                        {product.description}
                                    </Typography>
                                </CardContent>
                                <Grid container className={classes.priceButton}>
                                    <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
                                        { props.state.persistedReducer.product.addedProducts.filter(x => (x.id === product.id)).length === 0 &&
                                            <IconButton aria-label="remove" onClick={(event) => props.addProduct(product)}>
                                                <AddCircleOutlinedIcon className={classes.addB}/>
                                            </IconButton>
                                        }
                                        { props.state.persistedReducer.product.addedProducts.filter(x => (x.id === product.id)).length > 0 &&
                                            <IconButton aria-label="add" onClick={(event) => props.removeProduct(product)}>
                                                <RemoveCircleIcon color="primary"/>
                                            </IconButton>
                                        }
                                    </Grid>
                                    <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
                                        <Box display="flex" flexDirection="row-reverse" pt={2} pr={1} pb={1}>
                                            <Typography color="textSecondary">
                                                {product.price}
                                            </Typography>
                                        </Box>
                                    </Grid>
                                </Grid>
                            </div>
                        </Card>
                    </Grid>
                    ))
                }
            </Grid>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        addProduct: (val) => {
            dispatch(addProduct(val))
        },
        removeProduct: (val) => {
            dispatch(removeProduct(val))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);