import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import { selectChange } from "../slice/customerData"
import { connect } from "react-redux"
import { getProducts } from '../actions/index'

const properStyles = makeStyles((theme) => ({
    formControl: {
        float: "right",
        paddingRight: theme.spacing(2)
    },
}))

export const Sorter = (props) => {
    const classes = properStyles();
    return (
        <div>
            <FormControl className={classes.formControl} >
                <InputLabel id="filter">Filter</InputLabel>
                <Select labelId="filter" id="filterSelect" value={props.state.persistedReducer.customer.selectValue}
                onChange={(event) => props.selectChange(event.target.value, props)}>
                <MenuItem value="none">None</MenuItem>
                <MenuItem value="lowestPrice">Lowest Price</MenuItem>
                <MenuItem value="mostRecent">Most Recent</MenuItem>
                </Select>
                <FormHelperText>Pick your desired filter</FormHelperText>
            </FormControl>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        selectChange: (val, props) => {
            dispatch(selectChange(val))
            const uri = "http://localhost:3001/getProducts/"+val+"/"+props.state.persistedReducer.product.page;
            dispatch(getProducts(uri))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Sorter);