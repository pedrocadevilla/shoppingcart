import React from 'react'
import { Typography, makeStyles } from '@material-ui/core'
import { connect } from "react-redux"

const properStyles = makeStyles((theme) => ({
    errorMessage: {
        color: '#f54248',
        textAlign: 'right',
    }
}))

export const CustomerData = (props) => {
    const classes = properStyles()
    return (
        <div>
            <Typography variant="h4" color="initial" paragraph>
                Welcome back, { props.state.persistedReducer.customer.name }
            </Typography>
            <Typography variant="subtitle1" color="initial">
                ID: { props.state.persistedReducer.customer.id }
            </Typography>
            <Typography variant="subtitle1" color="initial">
                Address: { props.state.persistedReducer.customer.address }
            </Typography>
            <Typography variant="subtitle1" color="initial">
                Phone Number: { props.state.persistedReducer.customer.phoneNumber }
            </Typography>
            <Typography variant="subtitle1" color="initial">
                Email: { props.state.persistedReducer.customer.email }
            </Typography>
            <Typography variant="subtitle1" className={classes.errorMessage}>
                Not { props.state.persistedReducer.customer.name }? Look up again
            </Typography>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

export default connect(mapStateToProps)(CustomerData);