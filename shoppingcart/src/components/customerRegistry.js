import React from 'react'
import { makeStyles, Grid, Button, FormControl, TextField } from '@material-ui/core'
import { connect } from "react-redux"
import { Alert } from '@material-ui/lab';
import { addCustomerData } from '../actions/customerRegistry'
import { nameChange, idChange, addressChange, phoneNumberChange, emailChange } from "../slice/customerRegistry"
import { putError, removeError } from "../slice/customerRegistry"
import { getCustomerData } from '../actions/customerData'

const properStyles = makeStyles((theme) => ({
    formDiv: {
        minWidth: '100%',
        display: 'flex',
    },
    inputText: {
        marginBottom: theme.spacing(1)
    }
}))

export const CustomerRegistry = (props) => {
    const classes = properStyles()
    return (
        <div>
            <FormControl className={classes.formDiv}>
                { props.state.persistedReducer.customerRegistry.errorRegistry && <Alert severity="error">You have to fill in all the fields</Alert>}
                <TextField id="name" label="Full Name *" variant="outlined" className={classes.inputText} onChange={(event) => props.nameChange(event.target.value)}/>
                <TextField id="id" label="Id" variant="outlined" className={classes.inputText} onChange={(event) => props.idChange(event.target.value)}/>
                <TextField id="address" label="Address" variant="outlined" className={classes.inputText} onChange={(event) => props.addressChange(event.target.value)}/>
                <TextField id="phoneNumber" label="Phone Number" variant="outlined" className={classes.inputText} onChange={(event) => props.phoneNumberChange(event.target.value)}/>
                <TextField id="email" label="Email" variant="outlined" className={classes.inputText} onChange={(event) => props.emailChange(event.target.value)}/>
            </FormControl>
            <Grid container justify="flex-end">
                <Button variant="contained" color="secondary" onClick={(event) => props.addCustomerData(props.state.persistedReducer.customerRegistry)}>
                    Register
                </Button>
            </Grid>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        addCustomerData: (val) => {
            if(val.name !== "" && val.id !== "" && val.email !== "" && val.phoneNumber !== "" && val.address !== ""){
                dispatch(addCustomerData(val))
                dispatch(getCustomerData(val.email))
            }else{
                dispatch(putError())
            }
        },
        nameChange: (val) => {
            dispatch(nameChange(val))
            dispatch(removeError())
        },
        idChange: (val) => {
            dispatch(idChange(val))
            dispatch(removeError())
        },
        addressChange: (val) => {
            dispatch(addressChange(val))
            dispatch(removeError())
        },
        phoneNumberChange: (val) => {
            dispatch(phoneNumberChange(val))
            dispatch(removeError())
        },
        emailChange: (val) => {
            dispatch(emailChange(val))
            dispatch(removeError())
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerRegistry);