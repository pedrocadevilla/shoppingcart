import React from 'react'
import { makeStyles, Radio, RadioGroup, FormControlLabel, FormControl, FormLabel } from '@material-ui/core/'
import ExistingCustomer from './existingCustomer'
import CustomerData from './customerData'
import CustomerRegistry from './customerRegistry'
import { connect } from "react-redux"
import { radioChange } from "../slice/customerData"

const properStyles = makeStyles((theme) => ({
    formulary: {
        padding: theme.spacing(2)
    },
}))

export const CustomerInformation = (props) => {
    const classes = properStyles()
    return (
        <div>
            <FormControl className={classes.formulary}>
            <FormLabel component="legend">Are you?</FormLabel>
                <RadioGroup value={props.state.persistedReducer.customer.radioValue} onChange={(event) => props.radioChange(event.target.value)}>
                    <FormControlLabel value="new" control={<Radio />} label="New Customer" />
                    <FormControlLabel value="registered" control={<Radio />} label="Existing Customer" />
                </RadioGroup>
            </FormControl>
            { props.state.persistedReducer.customer.dataSearchVisible && (<ExistingCustomer/>) }
            { props.state.persistedReducer.customer.dataResultVisible && (<CustomerData/>) }
            { props.state.persistedReducer.customer.registryVisible && (<CustomerRegistry/>) }
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

const mapDispatchToProps = dispatch => {
    return {
        radioChange: (val) => {
            dispatch(radioChange(val))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerInformation);
