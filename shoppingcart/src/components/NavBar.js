import React from 'react'
import { AppBar, makeStyles, Toolbar } from '@material-ui/core'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import Badge from '@material-ui/core/Badge'
import { NavLink } from 'react-router-dom'
import { connect } from "react-redux";

const properStyles = makeStyles(theme => ({
    offset: theme.mixins.toolbar,
    menuButton: {
        marginRight: theme.spacing(2),
        color: '#FFFF',
    },
    title:{
        color: '#FFFF',
        textDecoration: 'none'
    },
    grow: {
        flexGrow: 1,
    },
    color: {
        color: '#FFFF'
    }
}))

export const NavBar = (props) => {
    const classes = properStyles()
    return (
        <div>
            <AppBar position="fixed" color="primary">
                <Toolbar>
                    <NavLink to="/" className="MuiTypography-root makeStyles-title-3 MuiTypography-h6">
                        Fast Shopping
                    </NavLink>
                    <div className={classes.grow} />
                    <NavLink to="/contact" className="MuiButtonBase-root MuiButton-root MuiButton-text makeStyles-menuButton-2">
                        Contact
                    </NavLink>
                    <NavLink to="/about" className="MuiButtonBase-root MuiButton-root MuiButton-text makeStyles-menuButton-2">
                        About
                    </NavLink>
                    <NavLink to="/placeOrder" className="MuiButtonBase-root MuiIconButton-root" aria-label="cart">
                        <Badge badgeContent={props.state.persistedReducer.product.addedProducts.length} className={classes.color}>
                            <ShoppingCartIcon className={classes.color}/>
                        </Badge>
                    </NavLink>
                </Toolbar>
            </AppBar>
            <div className={classes.offset}></div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        state
    };
}

export default connect(mapStateToProps)(NavBar);