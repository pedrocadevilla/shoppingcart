# ShoppingCart

ShoppingCart is an api that simulates a simple shop in a electronic store

## Installation

Download the files go to the shoppingcart/shoppingcart folder inside the downloadesd files using the console and use the comand yarn. Note (you have to do this also in the shoppingcartapi folder)

```bash
yarn
```

Also you have to install the DB using the script located in the folder model where you will find 2 scripts one with the schema of the database called "script.sql" and other with the data called "scriptData.sql" both scripts most be used in order to be able to use the application. Note (you need to have a MySQL server installed)

## Configuration

Before star using the api you have to update the ormconfig.jsom located in shoppingcart/shoppingcartapi/ in order to stablish a good connection to the DB 

```bash
{
    "name": "default",
    "type": "mysql",
    "host": "tourhost",
    "port": 3306,
    "username": "yourusername",
    "password": "yourpassword",
    "database": "shoppingcart",
    "schema": "yourschemaname",
    "synchronize": true,
    "entities": ["dist/entities/*.js"],
    "logging": false
}
```

## Usage

```python
You have to use 2 consoles 1 located in the shoppingcart/shoppingcartapi folder where you will run the command

"npm run build"

And the other console located in shoppingcart/shoppingcart folder where you will run the command

"yarn start"

After this you will be able to use the api using the direction 

"http://localhost:3000"